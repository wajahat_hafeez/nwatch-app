import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  data:any;
  baseUrl:string;
  constructor(public http: HttpClient) {
    console.log('Hello ModelProvider Provider');
  }

  getData(url) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }


    // don't have the data yet
  return new Promise((resolve,reject) => {

    var config = {
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
      },
      cache: true
  }
    
    this.http.get(url,config)
    .timeout(1000*10)
    .toPromise().then(data => {
        
        this.data = data;
        resolve(this.data);
      })
      .catch(error =>
        {
          console.log(error);
        reject(error);
      });
  });

  }


  postData(url, param) {

    var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

    // if (this.data) {
    //   // already loaded data
    //   return Promise.resolve(this.data);
    // }

    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }); 
    

    var configs = {
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
      }
  }
    
    // don't have the data yet
  return new Promise((resolve,reject) => {  
    
    this.http.post(url,param,configs)
    .timeout(1000*10)
    .toPromise().then(data => {
        
      console.log("Hello");
        this.data = data;
        resolve(this.data);
      })
      .catch(error =>
        {
          console.log(error);
          reject(error);
      });
  });

  }

}
