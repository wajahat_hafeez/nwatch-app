
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {
  
  static BASE_URL='http://nwatch.nayatel.com/NWatchApi/';

  static IS_LOGGED_IN : boolean = false;

  planName: string;

  liveStreamData:any;

  storageDays:number;

  constructor(platform: Platform,private storage: Storage) {
    console.log('Hello GlobalProvider Provider');
    // platform.ready().then(() => {
    //   console.log("UserID:"+this.getUserID())
    //   console.log("password:"+this.getUserPassword())
    //   console.log("Token:"+this.getUserToken())
    // });
  }

  
  //User subscribed Plan getter and setter
  getPlanValue() { return this.planName ; }
  setPlanValue(val) { this.planName = val; }

  //User cameras getter and setter
  getLiveStreamData() { return this.liveStreamData ; }
  setLiveStreamData(data) { this.liveStreamData = data; } 

  //Recording Allowed storage days getter and setter
  getAllowedStreamDays() { return this.storageDays ; }
  setAllowedStreamDays(data) { this.storageDays = data; } 

  // UserID getter and setter
  setUserID(userId:string)
  { 
    this.storage.set('userId', userId);
  }
  getUserID():string{
    var userID;
    this.storage.get('userId').then((val) => {

      userID = val;
      
    })

    return userID;
   }

  //User password getter and setter
  setUserPassowrd(password:string){ this.storage.set('password', password);}
  getUserPassword(): string{
    var pass;
    this.storage.get('password').then((val) => {

      pass = val;
      
    })

    return pass;
   }


   //User device token getter and setter
  setUserToken(token:string){ this.storage.set('fcm_token', token);}
  getUserToken(): string{
    var token;
    this.storage.get('fcm_token').then((val) => {

      token = val;
      
    })

    return token;
   }


}
