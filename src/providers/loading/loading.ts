import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingProvider {

  loader: any;

  constructor(public loadingCtrl: LoadingController) 
  {

  }

  public presentLoader(message?) {
    if (!message) {
      message = 'Loading...';
    }
    //return this.loadingCtrl.create({content: message});
    this.loader = this.loadingCtrl.create({content: message});
    this.loader.present();
  }

  public dismissLoader() {
    this.loader.dismiss();
  }



}
