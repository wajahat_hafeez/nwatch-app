import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, LoadingController, AlertController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePicker } from '@ionic-native/date-picker';
import { IonicStorageModule } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';

import { MyApp } from './app.component';
import { MenuPage } from '../pages/menu/menu';
import { TabsPage } from '../pages/tabs/tabs';
// import { SpecialPage } from '../pages/special/special';
import { LoginPage } from '../pages/login/login'; 
import { LivePage } from '../pages/live/live';
import { RecordingsPage } from '../pages/recordings/recordings';
import { FormsModule } from '@angular/forms';
import { ApiProvider } from '../providers/api/api';
import { HttpClientModule } from '@angular/common/http';
import { AlertProvider } from '../providers/alert/alert';
import { LoadingProvider } from '../providers/loading/loading';
import { GlobalProvider } from '../providers/global/global';
import { EventTrackingPage } from '../pages/event-tracking/event-tracking';
import { ProfilePage } from '../pages/profile/profile';
import { NotificationHandlerPage } from '../pages/notification-handler/notification-handler';

@NgModule({
  declarations: [
    MyApp,
    MenuPage,
    LoginPage,
    TabsPage,
    LivePage,
    RecordingsPage,
    EventTrackingPage,
    ProfilePage,
    NotificationHandlerPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,{
      mode: 'md'
      }),
      IonicStorageModule.forRoot(),
      
      BrowserAnimationsModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MenuPage,
    LoginPage,
    TabsPage,
    LivePage,
    RecordingsPage,
    EventTrackingPage,
    ProfilePage,
    NotificationHandlerPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AlertController,
    LoadingController,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    AlertProvider,
    LoadingProvider,
    GlobalProvider,
    DatePicker,
    StreamingMedia,
    FCM,
    LocalNotifications

  ]
})
export class AppModule {}
