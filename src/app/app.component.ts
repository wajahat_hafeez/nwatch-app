import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Storage } from '@ionic/storage';


import { LoginPage } from '../pages/login/login';
import { NotificationHandlerPage } from '../pages/notification-handler/notification-handler';
import { ApiProvider } from '../providers/api/api';
import { AlertProvider } from '../providers/alert/alert';
import { LoadingProvider } from '../providers/loading/loading';
import { GlobalProvider } from '../providers/global/global';
import { MenuPage } from '../pages/menu/menu';
@Component({
  templateUrl: 'app.html'
  
})
export class MyApp {
  
  @ViewChild(Nav) nav: Nav;
  rootPage : any;
  data;
  userid;
   pass;
  constructor(private localNotifications: LocalNotifications, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private fcm: FCM,  public api:ApiProvider, public alert: AlertProvider, public loader: LoadingProvider, private globals:GlobalProvider, private storage: Storage) {

    //this.rootPage = LoginPage
    platform.ready().then(() => {

      this.storage.get('userId').then((val) => {
      
        this.userid = val;
      })
      .catch((error) => 
      { 
        console.log("Error:"+error); 
      });

      this.storage.get('password').then((val) => {
        
        this.pass = val;
        
        const param = 'userid=' + this.userid + '&password=' + encodeURIComponent(this.pass);  
        console.log(param);

        let url = GlobalProvider.BASE_URL+'getAllStreams.php';

        this.loader.presentLoader('Logging you In...');
        this.api.postData(url, param)
        .then(data => {

          console.log(data);
          this.loader.dismissLoader();

          this.data = data;
          if(this.data.length === 0)
          {
            this.rootPage = LoginPage 
          }
          else if(this.data.length==1 && data[0].Responce === "Invalid Credentials")
          {
            this.rootPage = LoginPage
          }
          else if(this.data.length==1 && data[0].Responce === "Incomplete Information")
          {
            this.rootPage = LoginPage
          }
          else
          {
            // check Plan Name and save globally
            let plan = this.data['PlanName'];
            let fetchPlanAssinged = plan['PLANNAME'];
            this.globals.setPlanValue(fetchPlanAssinged === "NWATCH"? "NWATCH":"ET");

            //extract Cameras and save globally
            var cameras : any[] = [];
            let length = (Object.keys(this.data).length);

            for(var i=1 ; i < (length - 1) ; i++)
            {
              let cameraInfo = this.data[i];

              cameras.push(
                {title: cameraInfo['TITLE'], URL: cameraInfo['URL']}
              );
            }

            this.globals.setLiveStreamData(cameras);

            //set Number of days of which recordings are to be shown to the user.
            let storageDays = this.data['STORAGEDAYS'];
            let daysAllowed = storageDays['STORAGEDAYS'];
            this.globals.setAllowedStreamDays(daysAllowed);

            //set the session
            GlobalProvider.IS_LOGGED_IN = true;


            // register the fcm_token with this USERID(who has logged in)

            // const param = 'USERID=' + this.globals.getUserID() + '&TOKEN=' + this.globals.getUserToken();  

            // //let url = GlobalProvider.BASE_URL+'getAllStreams.php';

            // //this.loader.presentLoader('Logging you In...');
            // this.api.postData(url, param)
            // .then(data => {

            // })
            // .catch(error=>{
            //   console.log(error);
            // });

            // navigate to menu page
            this.rootPage = MenuPage

          }
          
        })
        .catch((error) =>
        {
          console.log(error);
          this.loader.dismissLoader();
          console.log("Error"+error.message)
          this.alert.show("Error", error.message);

        });       


              
            })
            .catch((error) => 
            { 
              console.log("Error:"+error); 
            });

  
      
    

    // let body = new HttpParams();

    //   body =  body.set('userid', this.userData.userid);
    //   body = body.set('password', encodeURIComponent(this.userData.password));

      

  

      // debugger;
      // this.rootPage = LoginPage

      // this.localNotifications.on('click').subscribe(data=>{

      //   console.log("Image which I want to parse:"+data['data'].data);
        
      //     this.nav.push(NotificationHandlerPage, { data: data['data'].data });
        
      //     console.log("Notification on click"+JSON.stringify(data))
      // });


      // this.fcm.subscribeToTopic('events');

      // this.fcm.getToken().then(token => {
      //   // backend.registerToken(token);

      //   //save firebase token in database
      //   this.globals.setUserToken(token);
      //   console.log("FCM TOKEN:"+token);
      // });


      // this.fcm.onNotification().subscribe(data => {
        
      //   console.log("Data Received on notification:"+JSON.stringify(data));
      //   if(data.wasTapped){

          
      //     console.log("Notification Data clicked while app was in background:"+JSON.stringify(data));
      //     console.log("Image Received:"+data.imagePath)
      //     this.nav.setRoot(NotificationHandlerPage, { data: data.imagePath });
      //     console.log("Received in background");
      //   } else {

          
      //     this.localNotifications.schedule({
      //       id: Math.floor((Math.random() * 10) + 1),
      //       title: data.title,
      //       text:data.body,
      //       trigger: {at: new Date(new Date().getTime())},
      //       data: { data: data.imagePath },
            
      //     });
  
      //     // Schedule a single notification
          
      //     //this.navCtrl.push(NotificationHandlerPage, { data: data });
      //     console.log("Received in foreground");
      //   }
      // });
      
      // this.fcm.onTokenRefresh().subscribe(token => {
      //   // backend.registerToken(token);

      //   //save firebase token in database
      //   this.globals.setUserToken(token);
        
      //   console.log("REFRESHED FCM TOKEN:"+token)
      // });
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

    });

    
  }

  
}

