import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

/**
 * Generated class for the Tab1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})
export class LivePage {

  cameras : any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider, private streamingMedia: StreamingMedia) {

    this.cameras = global.getLiveStreamData();
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Tab1Page');

  }

  openCam(index)
  {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      // orientation: 'portrait'
    };
    
    this.streamingMedia.playVideo(this.cameras[index].URL, options);
  }

}
