import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { GlobalProvider } from '../../providers/global/global';
import { AlertProvider } from '../../providers/alert/alert';
import { LoadingProvider } from '../../providers/loading/loading';

import { DatePicker } from '@ionic-native/date-picker';
import { Storage } from '@ionic/storage';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

import * as moment from 'moment';

/**
 * Generated class for the Tab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recordings',
  templateUrl: 'recordings.html',
  styles:[
    `
    .item-block{
      min-height: 0;
      transition: 0.09s all linear;
    }
    `
    ],
    animations: [
      trigger('expand', [
        state('true', style({ height: '45px' })),
        state('false', style({ height: '0'})),
        transition('void => *', animate('0s')),
        transition('* <=> *', animate('250ms ease-in-out'))
      ])
    ]
})
export class RecordingsPage {

  public groups = []
appName = 'Ionic App';
shownGroup;

  userid;
  password;
  //fullDate = new Date();
  date:any;
  mindate;
  maxdate;

  //Which date is selected by User
  selectedDay: number;

  //camera
  selectedCamera: string;

  //Allowed Days
  allowedDays : number = 1;

  data;


toggleGroup(group){
  group.active = !group.active

  if(group.active)
  {
    this.date = this.getStringFromDate(new Date());
    this.fetchCameraRecordings(this.selectedDay,group.title,group.id);
  }

}


  //all Cameras
  cameras : any[]=[];

  streamArray = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private globals: GlobalProvider, private loader:LoadingProvider, private alert:AlertProvider, private api: ApiProvider, private datePicker: DatePicker, private storage: Storage, private streamingMedia: StreamingMedia) {

    this.selectedDay = 1;

    this.date = this.getStringFromDate(new Date());
    this.getMinimumDate();

  }

  getStringFromDate(fulldate:any) : string
  {
    var twoDigitMonth = (fulldate.getMonth()+1) + "";
if (twoDigitMonth.length == 1)
    twoDigitMonth = "0" + twoDigitMonth;
var twoDigitDate = fulldate.getDate() + "";
if (twoDigitDate.length == 1)
    twoDigitDate = "0" + twoDigitDate;
var dateString = twoDigitDate + "-" + twoDigitMonth + "-" + fulldate.getFullYear(); 
return dateString
  }

  getMinimumDate()
  {
    var d = new Date();
    this.mindate = d.setDate(d.getDate() - (this.globals.getAllowedStreamDays()-1));

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad Tab2Page');

    this.fetchUserData()
    
  }

  ionViewWillEnter()
  {
    //console.log('ionViewWillEnter CameraStreamerPage');
    //fetch all Cameras
    this.cameras = this.globals.getLiveStreamData();  
    
    for(let i = 0; i < this.cameras.length; i++){
      this.groups[i]={
        active: false,
        id: i,
        title:this.cameras[i].title,
        items: []
      }
      // for (var j=0; j<3; j++) {
      //   this.groups[i].items.push(i + '-' + j);
      // }
    }

  }

  openDatePicker(index)
  {
    
    // console.log("Status of camera:"+this.groups[index].active)
    // console.log("Picker Displayed");
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      allowFutureDates:false,
      minDate: this.mindate,
      maxDate: new Date()
    })
    .then(date => {


        console.log('Got date: ', date)

        //compare current date and selected date to get the difference of days between two dates
        var todayDate = new Date();
        let today = this.getStringFromDate(todayDate);

        console.log("Today date:"+today);
        console.log("Selected Date:"+date);

        //set selected date to the view
        
        this.date = this.getStringFromDate(date);
        console.log(this.date);
        // find the difference
        var diff = this.calculateDateDifference(today,this.date)

        console.log("Difference is:"+diff);

          // As today represents 1, tomorrow represents 2 and so on so add +1 to match the api expected argument
          diff++;
  
        this.fetchCameraRecordings(diff,this.groups[index].title,index);
    })
    .catch(err => 
      {
        console.log('Error occurred while getting date: ', err)
      });
  }

  // getMonth(date:any) : number
  // {
  //   var twoDigitMonth = date.getMonth() + "";
  //   if (twoDigitMonth.length == 1)
  //     twoDigitMonth = "0" + twoDigitMonth;
    
  //   return parseInt(twoDigitMonth)
  // }

  calculateDateDifference(date1:string,date2:string): number
  {
    var a = moment(date1,'D/M/YYYY');
    var b = moment(date2,'D/M/YYYY');
    return moment.duration(a.diff(b)).asDays(); 
  }

  fetchUserData()
  {
    //fetch USERID AND PASSWORD if present in App Preferences
    this.storage.get('userId').then((val) => {
      this.userid = val;
      console.log('Your UserID is', val);
    })
    .catch((error) => 
    { 
      console.log("Error:"+error); 
    });

    this.storage.get('password').then((val) => {

      this.password = val;
      console.log('Your password is', val);
    })
    .catch((error) => 
    { 
      console.log("Error:"+error); 
    });
  }



  fetchCameraRecordings(streamday: number, streamname: string, tracker:number)
  {
    this.loader.presentLoader('Fetching Recording Files...');
    let URL = GlobalProvider.BASE_URL+'getAllRecs.php';

    let param = 'userid=nssipcam&password=' + encodeURIComponent('Ntl_n$$_38899') + '&streamday=' + streamday + '&streamname=' + streamname;
    console.log(param);


    this.api.postData(URL, param)
      .then(data => {

        this.loader.dismissLoader();
        this.data = data;
        if(this.data.length==1 && data[0].Responce === "Incomplete Information")
        {
          this.alert.show('Alert','Some fields are missing');
        }
        else if(this.data.length==1 && data[0].Responce === "Invalid Credentials")
        {
          this.alert.show('Alert','Invalid Credentials passed');
        }
        else
        {
          let length = (Object.keys(data).length);
          this.groups[tracker].items = [];
          this.streamArray = [];
  
          for (var j=0; j<length; j++) {
            let cameraInfo = this.data[j];

          
        this.groups[tracker].items.push(cameraInfo['title'])
        this.streamArray.push(cameraInfo['file']);

      }
        }
      })
      .catch(error=>{
        this.loader.dismissLoader();
        this.alert.show("Error",error);
        console.log(error)
      });

  }


  playRecording(camera,stream)
  {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      // orientation: 'portrait'
    };

    console.log("Camera index:"+camera);
    console.log("Item index:"+stream);
    
    console.log("Camera name:"+this.groups[camera].title);
    console.log("Time:"+this.groups[camera].items[stream]);
    console.log("Stream:"+this.streamArray[stream]);

    this.streamingMedia.playVideo(this.streamArray[stream], options);

  }

}
