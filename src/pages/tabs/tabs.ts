import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LivePage } from '../live/live';
import { RecordingsPage } from '../recordings/recordings';
import { EventTrackingPage } from '../event-tracking/event-tracking';
import { GlobalProvider } from '../../providers/global/global';
import { ProfilePage } from '../profile/profile';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',

})
export class TabsPage {

  plan: string = "";

  tab1Root = LivePage;
  tab2Root = RecordingsPage
  tab3Root = EventTrackingPage
  tab4Root = ProfilePage
  myIndex: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globals: GlobalProvider) {

    this.plan = globals.getPlanValue();
    console.log(this.plan);

    this.myIndex = navParams.data.tabIndex || 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
