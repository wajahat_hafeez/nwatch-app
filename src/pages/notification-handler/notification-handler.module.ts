import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationHandlerPage } from './notification-handler';

@NgModule({
  declarations: [
    NotificationHandlerPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationHandlerPage),
  ],
})
export class NotificationHandlerPageModule {}
