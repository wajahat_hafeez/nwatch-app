import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';

@IonicPage()
@Component({
  selector: 'page-notification-handler',
  templateUrl: 'notification-handler.html',
})
export class NotificationHandlerPage {

  image;
  ifLoggedIn = GlobalProvider.IS_LOGGED_IN;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationHandlerPage');
    var data = this.navParams.get('data')
    this.image = data
    console.log("Image path:"+this.image);
  }

  goToLogin()
  {
    this.navCtrl.setRoot(LoginPage);
  }

}
