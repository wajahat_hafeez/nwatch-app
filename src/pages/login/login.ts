import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { HttpParams } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { ApiProvider } from '../../providers/api/api';
import { AlertProvider } from '../../providers/alert/alert';
import { LoadingProvider } from '../../providers/loading/loading';
import { GlobalProvider } from '../../providers/global/global';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

  userData = { "userid": "","password":""};
  signinform: FormGroup;
  data;
  token:string;
  passwordType: string = 'password';
 passwordIcon: string = 'eye-off';

  constructor(public navCtrl: NavController, public navParams: NavParams, public api:ApiProvider, public alert: AlertProvider, public loader: LoadingProvider, public app:App, private globals:GlobalProvider, private storage: Storage) {
    //debugger;
    
  }

  ngOnInit()
  {
    //this.submitBeforeLogin();
    this.signinform = new FormGroup({
      
      userid: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(30)]),
      password: new FormControl('', [Validators.required])
      
    });
  }

  ionViewDidLoad(){
    //console.log('ionViewDidLoad LoginPage');
    
  }

  ionViewWillEnter(){
    // console.log("ionViewWillEnter--- LOGIN");
  }

  ionViewDidEnter(){
    //console.log('ionViewDidEnter LoginPage');
  }

  ionWillEnter(){
    //console.log("Ionic will enter--- LOGIN");
    this.app._setDisableScroll(false);
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
}


  submit()
  {
    const param = 'userid=' + this.userData.userid + '&password=' + encodeURIComponent(this.userData.password);  
    let url = GlobalProvider.BASE_URL+'getAllStreams.php';

    this.loader.presentLoader('Logging you In...');
    this.api.postData(url, param)
    .then(data => {

      console.log(data);
      this.loader.dismissLoader();

      this.data = data;
      if(this.data.length === 0)
      {
        this.alert.show('Information', 'Sorry. You have not subscribed the NWatch Service.');
      }
      else if(this.data.length==1 && data[0].Responce === "Invalid Credentials")
      {
        //
        this.alert.show('Login failed!', 'Please check your credentials.')
      }
      else if(this.data.length==1 && data[0].Responce === "Incomplete Information")
      {
        //
        this.alert.show('Login failed!', 'Some fields are missing.')
      }
      else
      {
        // check Plan Name and save globally
        let plan = this.data['PlanName'];
        let fetchPlanAssinged = plan['PLANNAME'];
        this.globals.setPlanValue(fetchPlanAssinged === "NWATCH"? "NWATCH":"ET");

        //extract Cameras and save globally
        var cameras : any[] = [];
        let length = (Object.keys(this.data).length);

        for(var i=1 ; i < (length - 1) ; i++)
        {
          let cameraInfo = this.data[i];

          cameras.push(
            {title: cameraInfo['TITLE'], URL: cameraInfo['URL']}
          );
        }

        this.globals.setLiveStreamData(cameras);

        //set Number of days of which recordings are to be shown to the user.
        let storageDays = this.data['STORAGEDAYS'];
        let daysAllowed = storageDays['STORAGEDAYS'];
        this.globals.setAllowedStreamDays(daysAllowed);

        // store in Storage
        this.storage.set('userId', this.userData.userid);
        this.storage.set('password', this.userData.password);

        //set the session
        GlobalProvider.IS_LOGGED_IN = true;


        // register the fcm_token with this USERID(who has logged in)

        const param = 'USERID=' + this.userData.userid + '&TOKEN=' + this.globals.getUserToken();  

        let url = GlobalProvider.BASE_URL+'getAllStreams.php';

        //this.loader.presentLoader('Logging you In...');
        this.api.postData(url, param)
        .then(data => {

        })
        .catch(error=>{

        });

        // navigate to menu page
        this.navCtrl.setRoot(MenuPage);

        //clear the fields

        this.userData.userid = '';
        this.userData.password = '';
      }
      
    })
    .catch((error) =>
    {
      console.log(error);
      this.loader.dismissLoader();
      console.log("Error"+error.message)
      this.alert.show("Error", error.message);

    });

  }

}
