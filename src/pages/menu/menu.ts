import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { RecordingsPage } from '../recordings/recordings';
import { LivePage } from '../live/live';
import { GlobalProvider } from '../../providers/global/global';
import { EventTrackingPage } from '../event-tracking/event-tracking';

import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 export interface PageInterface
 {
   title:string;
   pageName:any;
   tabComponent?:any;
   index?: number;
   icon:string;

 }

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  rootPage = TabsPage;
  @ViewChild(Nav) nav:Nav;

  data;
  
  pages: PageInterface[] = [
    {title: 'Live Streams', pageName: TabsPage, tabComponent: LivePage, index: 0,icon:'home'},
    {title: 'Recordings', pageName: TabsPage, tabComponent: RecordingsPage, index: 1,icon:'videocam'},
    {title: 'Profile', pageName: TabsPage, tabComponent: ProfilePage, index: 3,icon:'videocam'},
    {title: 'Logout', pageName: TabsPage,icon:'home'}
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams, private globals:GlobalProvider, private alertCtrl: AlertController, private storage: Storage) {

    if(globals.getPlanValue() != 'NWATCH')
    {
      this.pages.splice(2, 0, {title: 'Capture That Matters', pageName: TabsPage, tabComponent: EventTrackingPage, index: 2,icon:'home'});
    }
    else
    {
      this.pages[2].index = 2;
    }
    
    //this.cameras = globals.getLiveStreamData();
  }

  ionWillEnter()
  {
    console.log('ionWillEnter MenuPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');

  }

  openPage(page: PageInterface)
  {

    if(page.title == 'Logout')
    {
      
        let alert = this.alertCtrl.create({
          title: 'Confirmation',
          message: 'Are you sure you want to logout?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Logout',
              handler: () => {

                //clear the storage value and revert to login page
                this.storage.clear();
                this.navCtrl.setRoot(LoginPage);
                console.log('Logout clicked');
              }
            }
          ]
        });
        alert.present();
      }
    
    console.log(page.index);
    let params = {};

    if(page.index)
    {
      params = { tabIndex: page.index};
    }

    if(this.nav.getActiveChildNavs() && page.index != undefined)
    {
      this.nav.getActiveChildNav().select(page.index);
    }
    else{
      this.nav.setRoot(page.pageName, params);
    }
      
  }

  isActive(page: PageInterface)
  {
    let childNav = this.nav.getActiveChildNav();

    if(childNav)
    {
      if(childNav.getSelected() && childNav.getSelected().root === page.tabComponent)
      {
        return 'primary';
      }

      if(this.nav.getActive() && this.nav.getActive().name === page.pageName)
      {
        return 'primary';
      }
    }
  }

}
