import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventTrackingPage } from './event-tracking';

@NgModule({
  declarations: [
    EventTrackingPage,
  ],
  imports: [
    IonicPageModule.forChild(EventTrackingPage),
  ],
})
export class EventTrackingPageModule {}
